package acme;

import java.util.Optional;
import java.util.concurrent.Semaphore;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;
import cartago.*;

public class RoomDigitalTwinArtifact extends Artifact {

	private String uri;
	private HttpClient client;
	private int port;

	public void init(String uri, int port) {
		this.uri = uri;
		this.port = port;
		client = Vertx.vertx().createHttpClient();
		subscribe();
		log("ready.");
	}
	
	private void subscribe() {
		log("connecting to " + uri + ":" + port);
		client.websocket(port, uri, "/api/properties/room-state", ws -> {
			ws.handler(data -> {
				try {
					// log("notified new data " + data);
					String s = data.toString().substring(4);
					JsonObject obj = new JsonObject(s);
					/* generate SmartRoom percept */
					double temp = obj.getDouble("temperature");
					String state = obj.getString("state");
					this.beginExternalSession();
					ObsProperty tprop = getObsProperty("temperature");
					if (tprop == null) {
						defineObsProperty("temperature", temp);
					} else {
						tprop.updateValue(temp);
					}
					ObsProperty sprop = getObsProperty("state");
					if (sprop == null) {
						defineObsProperty("state",state);
					} else {
						sprop.updateValue(state);
					}
					this.endExternalSession(true);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			});		
		});
	}
	
	@OPERATION void getCurrentTemperature(OpFeedbackParam<Double> temp) {
		JsonObject obj = this.doGetBlocking("/api/properties/temperature");
		temp.set(obj.getDouble("temperature"));
	}

	@OPERATION void getCurrentState(OpFeedbackParam<String> state) {
		JsonObject obj = this.doGetBlocking("/api/properties/state");
		state.set(obj.getString("state"));
	}
	
	@OPERATION void startCooling() {
		this.doPostBlocking("/api/actions/start-cooling",Optional.empty());
	}

	@OPERATION void startHeating() {
		this.doPostBlocking("/api/actions/start-heating",Optional.empty());
	}

	@OPERATION void stopAirConditioner() {
		this.doPostBlocking("/api/actions/stop-working",Optional.empty());
	}

	private JsonObject doGetBlocking(String property) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		client.get(port, uri, property, response -> {
			response.bodyHandler(bodyHandler -> {
				JsonObject obj = bodyHandler.toJsonObject();
				result.set(obj);
				ready.release();
			});
		}).end();
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	private void doPostBlocking(String resource, Optional<JsonObject> body) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		if (!body.isPresent()) {
			client.post(port, uri, resource, response -> {
				// log("status: " + response.statusCode());
				ready.release();
			}).end();
		} else {
			client.post(port, uri, resource, response -> {
				ready.release();
			}).end(body.get().toBuffer());
		}
		try {
			ready.acquire();
		} catch (Exception ex) {
			// return null;
		}
	}
}
