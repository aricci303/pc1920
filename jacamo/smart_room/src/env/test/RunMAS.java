package test;

import java.io.File;

import jacamo.infra.*;

public class RunMAS {

	public static void main(String[] args) throws Exception {
		System.out.println("> " + new File(".").getAbsolutePath());
		// System.setProperty("user.dir", new File(".").getAbsolutePath()+"/jacamo/smart_room");
		// System.out.println("> " + new File(".").getAbsolutePath());
		jacamo.infra.JaCaMoLauncher.main(new String[] {"jacamo/smart_room/smart_room_step4.jcm"});
	}

}
