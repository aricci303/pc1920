// CArtAgO artifact code for project first

package acme;

import cartago.*;

public class MyCounter extends Artifact {
	
	void init(int initialValue) {
		defineObsProperty("count", initialValue);
	}

	@OPERATION
	void inc() {
		ObsProperty prop = getObsProperty("count");
		prop.updateValue(prop.intValue()+1);
		signal("tick");
	}
}

