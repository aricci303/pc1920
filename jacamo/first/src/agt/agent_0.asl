my_bel(15).


!my_goal(20).


+!my_goal(X) : my_bel(Y) & X > Y
  <- println("a1 ", X);
     ?my_bel(Y);
     println("a2 ", Y).

     
-!my_goal(X) 
	<- println("recovered.").
		
{ include("$jacamoJar/templates/common-cartago.asl") }
{ include("$jacamoJar/templates/common-moise.asl") }
