package smart_room.version2.thermostat_agent3;

public class UserStartedPercept implements Percept {
	
	private long time;
	
	public UserStartedPercept(long time) {
		this.time = time;
	}

	public long getTimestamp() {
		return time;
	}
	
	public String toString() {
		return "UserStated: { time: " + time + "}";
	}
}
