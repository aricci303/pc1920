package smart_room.version2.thermostat_agent3;

public class RoomModel {

	private double temperature;
	private String state;
	private boolean available;
	
	public RoomModel() {
		available = false;
	}
	
	public synchronized double getTemperature() {
		return temperature;
	}

	public synchronized String getState() {
		return state;
	}

	public synchronized boolean isAvailable() {
		return available;		
	}
	
	public void update(String state, double temp) {
		this.state = state;
		this.temperature = temp;
		available = true;
	}
	
}
