package smart_room.version2.thermostat_agent3;

import java.util.Optional;
import java.util.concurrent.Semaphore;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;

public class RoomDigitalTwinInterface extends Observable {

	private String uri;
	private HttpClient client;
	private int port;

	public RoomDigitalTwinInterface(String uri, int port) {
		this.uri = uri;
		this.port = port;
		client = Vertx.vertx().createHttpClient();
		
	}
	
	public void subscribe() {
		client.websocket(port, uri, "/api/properties/room-state", ws -> {
			ws.handler(data -> {
				String s = data.toString().substring(4);
				JsonObject obj = new JsonObject(s);
				/* generate SmartRoom percept */
				double temp = obj.getDouble("temperature");
				String state = obj.getString("state");
				this.notifyEvent(new ChangedSmartRoomPercept(state, temp));
			});		
		});
	}
	
	protected double getCurrentTemperature() {
		JsonObject obj = this.doGetBlocking("/api/properties/temperature");
		return obj.getDouble("temperature");
	}

	protected String getCurrentState() {
		JsonObject obj = this.doGetBlocking("/api/properties/state");
		return obj.getString("state");
	}
	
	protected void startCooling() {
		this.doPostBlocking("/api/actions/start-cooling",Optional.empty());
	}

	protected void startHeating() {
		this.doPostBlocking("/api/actions/start-heating",Optional.empty());
	}

	protected void stopWorking() {
		this.doPostBlocking("/api/actions/stop-working",Optional.empty());
	}

	private JsonObject doGetBlocking(String property) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		client.get(port, uri, property, response -> {
			response.bodyHandler(bodyHandler -> {
				JsonObject obj = bodyHandler.toJsonObject();
				result.set(obj);
				ready.release();
			});
		}).end();
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	private void doPostBlocking(String resource, Optional<JsonObject> body) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		if (!body.isPresent()) {
			client.post(port, uri, resource, response -> {
				log("status: " + response.statusCode());
				ready.release();
			}).end();
		} else {
			client.post(port, uri, resource, response -> {
				ready.release();
			}).end(body.get().toBuffer());
		}
		try {
			ready.acquire();
		} catch (Exception ex) {
			// return null;
		}
	}
	
	private void log(String msg) {
		System.out.println("[ROOM DT INTERFACE] " + msg);
	}
}
