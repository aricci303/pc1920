package smart_room.version2.thermostat_agent3;

import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;

public class ThermostatAgent extends Thread implements Observer {

	private static final int defaultEventQueueSize = 10000;
	protected BlockingQueue<Percept> perceptQueue;
	private static final int SENSING_PERIOD = 50;
		
	private 	UserPrefPanel panel;
	private RoomDigitalTwinInterface roomdt;
	
	/* current knowledge about the world */

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";

	private RoomModel roomModel;
	private UserModel userModel;
	
	private String roomURI;
	private int roomPort;

	public ThermostatAgent(String roomURL, int port, double targetGoalTemperature) {

		this.roomURI = roomURL;
		this.roomPort = port;
		
		roomModel = new RoomModel();
		userModel = new UserModel(targetGoalTemperature);
	}
	
	public void spawn() {
		perceptQueue = new ArrayBlockingQueue<Percept>(defaultEventQueueSize);
		this.setupSensingCycle();
		start();
	}
	
	public void run() {
		log("launched.");
		
		log("setting up GUI.");
		createUserGUI(21);
		
		log("waiting for user start.");
		startObservingUser();

		while (!userModel.userStarted()) {
			waitTimeInMs(100);
		}
		
		log("Connecting to room: " + roomURI);		
		createRoomDTInterface(roomURI, roomPort);
		
		startObservingRoom();	
		
		log("waiting for data.");

		while (!roomModel.isAvailable()) {
			waitTimeInMs(100);
		}
		
		while (true) {
			
			/* this could become an atomic block */
			double currentTemp = roomModel.getTemperature();
			String state = roomModel.getState();
			double targetGoalTemperature = userModel.getPreferredTemperature();
			/* ---- */
			
			if (Math.abs(currentTemp - targetGoalTemperature) > THRESOLD) {
				log("Room temperature perceived (" + currentTemp + ") different from target one (" + targetGoalTemperature + ") - ACTING" );
				boolean tempAchieved = false;
				while (!tempAchieved) {

					/* this could become an atomic block */
					currentTemp = roomModel.getTemperature();
					state = roomModel.getState();
					targetGoalTemperature = userModel.getPreferredTemperature();
					/* ---- */

					log("ACTING - current temperature: " + currentTemp + " - state: " + state +") - ACTING" );
					if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
						log("too cold: start heating...");
						roomdt.startHeating();
					} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
						log("too hot: start cooling...");
						roomdt.startCooling();
					} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
						log("achieved: stop working");
						if (!state.equals(IDLE)) {
							roomdt.stopWorking();
						}
						tempAchieved = true;
					}
					waitTimeInMs(500);
				}
			} else {
				log("Room temperature perceived (" + currentTemp + ") not different from target one (" + targetGoalTemperature + ") - NOTHING TO DO." );
			}
			waitTimeInMs(1000);
		}
	}
		
	
	// actions
	
	protected void createRoomDTInterface(String url, int port) {
		this.roomdt = new RoomDigitalTwinInterface(url, port);
	}
	
	protected void createUserGUI(int defPrefTemp) {
		panel = new 	UserPrefPanel(defPrefTemp);
		panel.display();
	}
	
	private void startObservingRoom() {
		roomdt.addObserver(this);
		roomdt.subscribe();
	}

	private void startObservingUser() {
		panel.addObserver(this);
	}

	
	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}
	
	protected void waitTimeInMs(long dt) {
		try {
			Thread.sleep(dt);
		} catch (Exception ex) {};
	}
	
	// related to the execution cycle
	
	/* setting up the sensing cycle, async in this case */
	
	private void setupSensingCycle() {
		new Thread(() -> {
			while (true) {
				sense();
				try {
					Thread.sleep(SENSING_PERIOD);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}).start();
	}
	
	private void sense() {
		Percept p = this.perceptQueue.poll();
		if (p != null) {
			log("New percept: " + p);
			if (p instanceof ChangedSmartRoomPercept) {
				ChangedSmartRoomPercept smp = (ChangedSmartRoomPercept) p;
				roomModel.update(smp.getState(), smp.getTemperature());
			} else if (p instanceof NewPrefTempPercept) {
				NewPrefTempPercept nt = (NewPrefTempPercept) p;
				userModel.updatePrefTemp(nt.getTemperature());
			} else if (p instanceof UserStartedPercept) {
				UserStartedPercept us = (UserStartedPercept) p;
				userModel.updateUserStarted(true);
			} else {
				log("unknown percept: " + p);
			}
		} 
	}

	
	// aux
		

	@Override
	public void notifyEvent(Percept ev) {
		try {
			perceptQueue.put(ev);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}	

	public static void main(String[] args) {
		new ThermostatAgent("localhost", 12000, 21).spawn();
	}

}
