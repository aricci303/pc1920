package smart_room.version2.thermostat_agent3;

public interface Observer {

	void notifyEvent(Percept ev);
}
