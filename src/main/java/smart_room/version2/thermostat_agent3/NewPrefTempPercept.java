package smart_room.version2.thermostat_agent3;

public class NewPrefTempPercept implements Percept {
	
	private int temperature;
	
	public NewPrefTempPercept(int value) {
		this.temperature = value;
	}

	public int getTemperature() {
		return temperature;
	}
	
	public String toString() {
		return "NewPrefTempPercept: { temperature: " + temperature + "}";
	}
	
}
