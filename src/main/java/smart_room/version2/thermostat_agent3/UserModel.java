package smart_room.version2.thermostat_agent3;

public class UserModel {

	private double prefTemp;
	private boolean userStarted;
	
	public UserModel(double prefTemp) {		
		this.prefTemp = prefTemp;
		userStarted = false;
	}
	
	public synchronized double getPreferredTemperature() {
		return prefTemp;
	}
	
	public synchronized boolean userStarted() {
		return userStarted;
	}
	
	public synchronized void updatePrefTemp(double value) {
		this.prefTemp = value;
	}
	
	public synchronized void updateUserStarted(boolean started) {
		this.userStarted = started;
	}
}
