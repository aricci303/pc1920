package smart_room.version2.thermostat_agent2;

public class ChangedSmartRoomPercept implements Percept {

	private double temperature;
	private String state;
	
	public ChangedSmartRoomPercept(String state, double temperature) {
		this.temperature = temperature;
		this.state = state;
	}
	
	public double getTemperature() {
		return temperature;
	}

	public String getState() {
		return state;
	}

}
