package smart_room.version2.thermostat_agent2;

public class RoomModel {

	private double temperature;
	private String state;
	private boolean available;
	
	public RoomModel() {
		available = false;
	}
	
	public double getTemperature() {
		return temperature;
	}

	public String getState() {
		return state;
	}

	public boolean isAvailable() {
		return available;		
	}
	
	public void update(String state, double temp) {
		this.state = state;
		this.temperature = temp;
		available = true;
	}
	
}
