package smart_room.version2.thermostat_agent2;

public interface Observer {

	void notifyEvent(Percept ev);
}
