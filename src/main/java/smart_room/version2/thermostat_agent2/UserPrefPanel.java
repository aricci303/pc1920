package smart_room.version2.thermostat_agent2;

public class UserPrefPanel extends Observable {

	private UserPrefPanelFrame frame;
	
	public UserPrefPanel() {
		frame = new UserPrefPanelFrame(this);
	}
	
	public void display() {
		frame.display();
	}
	
	void notifyChangeTemp(int newValue) {
		this.notifyEvent(new NewPrefTempPercept(newValue));
	}
	
	void notifyStarted() {
		this.notifyEvent(new UserStartedPercept(System.currentTimeMillis()));
	}
}
