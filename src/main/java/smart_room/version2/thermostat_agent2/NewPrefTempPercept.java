package smart_room.version2.thermostat_agent2;

public class NewPrefTempPercept implements Percept {
	
	private int temperature;
	
	public NewPrefTempPercept(int value) {
		this.temperature = value;
	}

	public int getTemperature() {
		return temperature;
	}
}
