package smart_room.version2.thermostat_agent2;

public class Holder<T> {

	private T obj;
	
	public Holder() {
	}
		
	public void set(T obj) {
		this.obj = obj;
	}
	
	public T get() {
		return obj;
	}
}
