package smart_room.version2.thermostat_agent2;

import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;

public class ThermostatAgentStep3 extends Thread implements Observer {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	
	private String smartRoomURL;
	private HttpClient client;
	private int port;
	
	private 	UserPrefPanel panel;

	/* current knowledge about the world */
	private RoomModel model;
	private double targetGoalTemperature;
	private boolean userStarted;
	
	private static final int defaultEventQueueSize = 1000;
	protected BlockingQueue<Percept> perceptQueue;


	public ThermostatAgentStep3(String smartRoomURL, int port, double targetGoalTemperature) {
		perceptQueue = new ArrayBlockingQueue<Percept>(defaultEventQueueSize);

		this.targetGoalTemperature = targetGoalTemperature;
		this.userStarted = false;
		
		this.smartRoomURL = smartRoomURL;
		this.port = port;
		client = Vertx.vertx().createHttpClient();
		model = new RoomModel();
	}
	
	public void run() {
		log("launched - working with " + smartRoomURL);		

		log("setting up GUI.");
		createUserGUI();
		
		log("waiting for user start.");
		startObservingUser();

		while (!userStarted) {
			sense();
			waitTimeInMs(100);
		}
		
		startObservingRoom();	
		log("waiting for data.");
		
		while (!model.isAvailable()) {
			sense();
			waitTimeInMs(100);
		}
		
		while (true) {
			sense();
			
			double currentTemp = model.getTemperature();
			String state = model.getState();

			log("Idle - State: current temperature: " + currentTemp + " - state: " + state );
			if (Math.abs(currentTemp - targetGoalTemperature) > THRESOLD) {
				boolean tempAchieved = false;
				while (!tempAchieved) {
					sense();

					currentTemp = model.getTemperature();
					state = model.getState();

					log("Acting (State: current temperature: " + currentTemp + " - state: " + state +")" );
					if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
						log("too cold: start heating...");
						startHeating();
					} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
						log("too hot: start cooling...");
						startCooling();
					} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
						log("achieved: stop working");
						if (!state.equals(IDLE)) {
							stopWorking();
						}
						tempAchieved = true;
					}
					waitTimeInMs(100);
				}
			}
			waitTimeInMs(100);
		}
	}
	
	
	// actions
	
	protected void createUserGUI() {
		panel = new 	UserPrefPanel();
		panel.display();
	}
	
	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}
	
	protected void waitTimeInMs(long dt) {
		try {
			Thread.sleep(dt);
		} catch (Exception ex) {};
	}
	
	protected double getCurrentTemperature() {
		JsonObject obj = this.doGetBlocking("/api/properties/temperature");
		return obj.getDouble("temperature");
	}

	protected String getCurrentState() {
		JsonObject obj = this.doGetBlocking("/api/properties/state");
		return obj.getString("state");
	}
	
	protected void startCooling() {
		this.doPostBlocking("/api/actions/start-cooling",Optional.empty());
	}

	protected void startHeating() {
		this.doPostBlocking("/api/actions/start-heating",Optional.empty());
	}

	protected void stopWorking() {
		this.doPostBlocking("/api/actions/stop-working",Optional.empty());
	}
	
	private void startObservingRoom() {
		client.websocket(port, smartRoomURL, "/api/properties/room-state", ws -> {
			ws.handler(data -> {
				String s = data.toString().substring(4);
				JsonObject obj = new JsonObject(s);
				/* generate SmartRoom percept */
				double temp = obj.getDouble("temperature");
				String state = obj.getString("state");
				this.perceptQueue.add(new ChangedSmartRoomPercept(state, temp));
			});		
		});
	}

	private void startObservingUser() {
		panel.addObserver(this);
	}

	// related to the execution cycle
	
	void sense() {
		Percept p = this.perceptQueue.poll();
		if (p != null) {
			log("New percept: " + p);
			if (p instanceof ChangedSmartRoomPercept) {
				ChangedSmartRoomPercept smp = (ChangedSmartRoomPercept) p;
				model.update(smp.getState(), smp.getTemperature());
			} else if (p instanceof NewPrefTempPercept) {
				NewPrefTempPercept nt = (NewPrefTempPercept) p;
				targetGoalTemperature = nt.getTemperature();
			} else if (p instanceof UserStartedPercept) {
				UserStartedPercept us = (UserStartedPercept) p;
				userStarted = true;
			} else {
				log("unknown percept: " + p);
			}
		} 
	}
	
	// aux
		
	private JsonObject doGetBlocking(String property) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		client.get(port, smartRoomURL, property, response -> {
			response.bodyHandler(bodyHandler -> {
				JsonObject obj = bodyHandler.toJsonObject();
				result.set(obj);
				ready.release();
			});
		}).end();
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	private void doPostBlocking(String resource, Optional<JsonObject> body) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		if (!body.isPresent()) {
			client.post(port, smartRoomURL, resource, response -> {
				log("status: " + response.statusCode());
				ready.release();
			}).end();
		} else {
			client.post(port, smartRoomURL, resource, response -> {
				ready.release();
			}).end(body.get().toBuffer());
		}
		try {
			ready.acquire();
		} catch (Exception ex) {
			// return null;
		}
	}

	@Override
	public void notifyEvent(Percept ev) {
		try {
			perceptQueue.put(ev);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}	

	public static void main(String[] args) {
		new ThermostatAgentStep3("localhost", 12000, 21).start();
	}

}
