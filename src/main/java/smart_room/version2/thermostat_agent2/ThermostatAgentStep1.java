package smart_room.version2.thermostat_agent2;

import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;

public class ThermostatAgentStep1 extends Thread {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	
	private double targetGoalTemperature;
	private String smartRoomURL;
	private HttpClient client;
	private int port;
	
	private RoomModel model;

	private static final int defaultEventQueueSize = 50;
	protected BlockingQueue<Percept> perceptQueue;

	public ThermostatAgentStep1(String smartRoomURL, int port, double targetGoalTemperature) {
		perceptQueue = new ArrayBlockingQueue<Percept>(defaultEventQueueSize);

		this.targetGoalTemperature = targetGoalTemperature;
		this.smartRoomURL = smartRoomURL;
		this.port = port;
		client = Vertx.vertx().createHttpClient();
		model = new RoomModel();
	}
	
	public void run() {
		log("launched - working with " + smartRoomURL + " - temperature to achieve: " + targetGoalTemperature);		
		
		startObserving();	
		
		while (!model.isAvailable()) {
			sense();
			log("Waiting for data.");
			waitTimeInMs(100);
		}
		
		while (true) {
			sense();
			
			double currentTemp = model.getTemperature();
			String state = model.getState();
			log("State: current temperature: " + currentTemp + " - state: " + state );
			if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
				log("too cold: start heating...");
				startHeating();
			} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
				log("too hot: start cooling...");
				startCooling();
			} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
				log("achieved: stop working");
				if (!state.equals(IDLE)) {
					stopWorking();
				}
				break;
			}
			waitTimeInMs(1000);
		}
		log("GOAL ACHIEVED.");
	}
	
	// actions
	
	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}
	
	protected void waitTimeInMs(long dt) {
		try {
			Thread.sleep(dt);
		} catch (Exception ex) {};
	}
	
	protected double getCurrentTemperature() {
		JsonObject obj = this.doGetBlocking("/api/properties/temperature");
		return obj.getDouble("temperature");
	}

	protected String getCurrentState() {
		JsonObject obj = this.doGetBlocking("/api/properties/state");
		return obj.getString("state");
	}
	
	protected void startCooling() {
		this.doPostBlocking("/api/actions/start-cooling",Optional.empty());
	}

	protected void startHeating() {
		this.doPostBlocking("/api/actions/start-heating",Optional.empty());
	}

	protected void stopWorking() {
		this.doPostBlocking("/api/actions/stop-working",Optional.empty());
	}
	
	private void startObserving() {
		client.websocket(port, smartRoomURL, "/api/properties/room-state", ws -> {
			ws.handler(data -> {
				String s = data.toString().substring(4);
				JsonObject obj = new JsonObject(s);
				System.out.println("New percept: " + obj);

				/* generate SmartRoom percept */
				double temp = obj.getDouble("temperature");
				String state = obj.getString("state");
				this.perceptQueue.add(new ChangedSmartRoomPercept(state, temp));
			});		
		});
	}

	// related to the execution cycle
	
	void sense() {
		Percept p = this.perceptQueue.poll();
		if (p != null) {
			ChangedSmartRoomPercept smp = (ChangedSmartRoomPercept) p;
			model.update(smp.getState(), smp.getTemperature());
		}
	}
	
	// aux
		
	
	private JsonObject doGetBlocking(String property) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		client.get(port, smartRoomURL, property, response -> {
			response.bodyHandler(bodyHandler -> {
				JsonObject obj = bodyHandler.toJsonObject();
				result.set(obj);
				ready.release();
			});
		}).end();
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	private void doPostBlocking(String resource, Optional<JsonObject> body) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		if (!body.isPresent()) {
			client.post(port, smartRoomURL, resource, response -> {
				log("status: " + response.statusCode());
				ready.release();
			}).end();
		} else {
			client.post(port, smartRoomURL, resource, response -> {
				ready.release();
			}).end(body.get().toBuffer());
		}
		try {
			ready.acquire();
		} catch (Exception ex) {
			// return null;
		}
	}

	public static void main(String[] args) {
		new ThermostatAgentStep1("localhost", 12000, 21).start();
	}	
}
