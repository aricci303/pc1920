package smart_room.version2.room_digitaltwin;

import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

public class RoomSimulator {

	private RoomModel model;
	private RoomDigitalTwin dt;
	private final ScheduledExecutorService scheduler =
		     Executors.newScheduledThreadPool(1);
	private Optional<ScheduledFuture<?>> handle = Optional.empty();
	private RoomSimulatorPanel panel;
	
	public RoomSimulator(RoomDigitalTwin dt) {
		this.dt = dt;
	    this.model = new RoomModel();
	    model.setState("idle");
	    model.setTemperature(21);
		dt.align(model);
	    
	    panel = new RoomSimulatorPanel(this);
	    SwingUtilities.invokeLater(() -> {
	    		panel.setVisible(true);
	    });
	}
	
	public void startHeating(int period) {
	    if (handle.isPresent()) {
	    		handle.get().cancel(true);
	    }
	    model.setState("heating");
	    dt.align(model);
		handle = Optional.of(scheduler.scheduleAtFixedRate(() -> {
			double t = model.getTemperature();
			model.setTemperature(t + 0.1);	
			dt.align(model);
			panel.updateTemp();
	     }, 0, period, TimeUnit.MILLISECONDS));
	}
	
	public void startCooling(int period) {
	    if (handle.isPresent()) {
    			handle.get().cancel(true);
	    }
	    model.setState("cooling");
		dt.align(model);
	    handle = Optional.of(scheduler.scheduleAtFixedRate(() -> {
	    		double t = model.getTemperature();
	    		model.setTemperature(t - 0.1);	
			dt.align(model);
			panel.updateTemp();
	    }, 0, period, TimeUnit.MILLISECONDS));
	}
	
	public void stopWorking() {
	   if (handle.isPresent()) {
	    		handle.get().cancel(true);
	    		handle = Optional.empty();
	    	    model.setState("idle");
			dt.align(model);
	   }
	}
	
	public void notifyNewTemperature(double temp) {
		model.setTemperature(temp);	
		dt.align(model);
		panel.updateTemp();
	}
	
	public RoomModel getModel() {
		return model;
	}
}
