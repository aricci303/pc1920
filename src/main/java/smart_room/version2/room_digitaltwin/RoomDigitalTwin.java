package smart_room.version2.room_digitaltwin;

import java.util.Optional;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.http.ServerWebSocket;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;

public class RoomDigitalTwin extends AbstractVerticle {

	private int localPort;

	private RoomModel model;
	private RoomPhysicalAssetBridge physicalAsset;
	private 	Vertx vertx;
	private Optional<ServerWebSocket> ws = Optional.empty();
	
	public RoomDigitalTwin(int port) {
		this.localPort = port;	
	}

	@Override
	public void start() {	
		vertx = Vertx.vertx();
		model = new RoomModel();
		physicalAsset = new RoomPhysicalAssetBridge(this);		
		setup();
		log("Ready");
	}

	private void setup() {
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		
		/* obs prop */
		router.get("/api/properties/temperature").handler(this::handleGetTemperature);
		router.get("/api/properties/state").handler(this::handleGetState);
		
		/* actions */
		router.post("/api/actions/start-heating").handler(this::handleStartHeating);
		router.post("/api/actions/start-cooling").handler(this::handleStartCooling);
		router.post("/api/actions/stop-working").handler(this::handleStopWorking);

		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.websocketHandler(this::webSocketHandler)			
			.listen(localPort);
	}
	
	private void handleGetTemperature(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
		JsonObject obj = new JsonObject();
		obj.put("temperature", model.getTemperature());
		response.putHeader("content-type", "application/json").end(obj.encodePrettily());
	}

	private void handleGetState(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
		JsonObject obj = new JsonObject();
		obj.put("state", model.getState());
		response.putHeader("content-type", "application/json").end(obj.encodePrettily());
	}
	
	private void handleStartHeating(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
		physicalAsset.doCmdStartHeating();
		response.end("");
	}

	private void handleStartCooling(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
		physicalAsset.doCmdStartCooling();
		response.end("");
	}
	
	private void handleStopWorking(RoutingContext ctx) {
		HttpServerResponse response = ctx.response();
		physicalAsset.doCmdStopWorking();
		response.end("");
	}
	
	private void webSocketHandler(ServerWebSocket ws) {
		 if (ws.path().equals("/api/properties/room-state")) {
			 this.ws = Optional.of(ws);
			 log("new subscription - " + ws.remoteAddress());
			 publishChange();
		 } else {
			 ws.reject();
		 }
	}
	
	public void align(RoomModel model) {
		this.model.setState(model.getState());
		this.model.setTemperature(model.getTemperature());
		publishChange();
	}

	private void publishChange() {
		if (ws.isPresent()) {
			// log("publish change.");
			JsonObject obj = new JsonObject();
			obj.put("temperature", model.getTemperature());
			obj.put("state", model.getState());
			Buffer buffer = Buffer.buffer();
			obj.writeToBuffer(buffer);
			ws.get().write(buffer);
		}		
	}
	public void log(String msg) {
		System.out.println("[SMART ROOM DT] "+msg);
	}

	public static void main(String[] args) {
		new RoomDigitalTwin(12000).start();
	}
	
}