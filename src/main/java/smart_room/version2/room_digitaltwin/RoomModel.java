package smart_room.version2.room_digitaltwin;

public class RoomModel {

	private double temperature;
	private String state;
	
	public RoomModel() {
	}
	
	public synchronized double getTemperature() {
		return temperature;
	}

	public synchronized void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	public synchronized String getState() {
		return state;
	}

	public synchronized void setState(String state) {
		this.state = state;
	}
	
}
