package smart_room.version2.room_digitaltwin;


public class RoomPhysicalAssetBridge {

	private RoomDigitalTwin dt;
	private RoomSimulator physicalAssetSimulator;
	
	public RoomPhysicalAssetBridge(RoomDigitalTwin dt) {
		this.dt = dt;
		this.physicalAssetSimulator = new RoomSimulator(dt);
	}
	
	public void doCmdStartHeating() {
		physicalAssetSimulator.startHeating(100);
	}
	
	public void doCmdStartCooling() {
		physicalAssetSimulator.startCooling(100);
	}
	
	public void doCmdStopWorking() {
		physicalAssetSimulator.stopWorking();
	}
}
