package smart_room.version2.thermostat_agent;

public class SmartRoomModel {

	private double temperature;
	private String state;
	private boolean available;
	
	public SmartRoomModel() {
		available = false;
	}
	
	public synchronized double getTemperature() {
		return temperature;
	}

	public synchronized String getState() {
		return state;
	}

	public synchronized boolean isAvailable() {
		return available;		
	}
	
	public synchronized void update(String state, double temp) {
		this.state = state;
		this.temperature = temp;
		available = true;
	}
	
}
