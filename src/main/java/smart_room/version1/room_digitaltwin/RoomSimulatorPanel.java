package smart_room.version1.room_digitaltwin;

import java.awt.Dimension;

import javax.swing.*;
import javax.swing.event.ChangeEvent;

class RoomSimulatorPanel extends JFrame {		
	
	private JTextField tempValue;
	private JSlider temp;
	private RoomModel model;
	
	public RoomSimulatorPanel(RoomModel model){
		setTitle("..:: Room Simulator Panel ::..");
		this.model = model;
		setSize(400,120);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		setContentPane(mainPanel);
		
		JPanel temperature = new JPanel();
		temperature.setLayout(new BoxLayout(temperature, BoxLayout.Y_AXIS));

		JPanel temperature1 = new JPanel();
		temperature1.setLayout(new BoxLayout(temperature1, BoxLayout.X_AXIS));
		
		tempValue = new JTextField(5);
		tempValue.setText("" + model.getTemperature());
		tempValue.setSize(100, 30);
		tempValue.setMinimumSize(tempValue.getSize());
		tempValue.setMaximumSize(tempValue.getSize());
		tempValue.setEditable(false);
		
		temperature1.add(new JLabel("Room physical temperature:"));
		temperature1.add(Box.createRigidArea(new Dimension(0,5)));
		temperature1.add(tempValue);
		
		temp = new JSlider(JSlider.HORIZONTAL, 5, 45, (int) model.getTemperature());
		temp.setSize(300, 60);
		temp.setMinimumSize(temp.getSize());
		temp.setMaximumSize(temp.getSize());
		temp.setMajorTickSpacing(10);
		temp.setMinorTickSpacing(1);
		temp.setPaintTicks(true);
		temp.setPaintLabels(true);
		
		temp.addChangeListener((ChangeEvent ev) -> {
			model.setTemperature( temp.getValue());
			tempValue.setText("" + model.getTemperature());
		});

		temperature.add(temperature1);
		temperature.add(temp);
		
		mainPanel.add(temperature);
	}
	
	public void updateTemp() {
		SwingUtilities.invokeLater(() -> {
			tempValue.setText(String.format("%.2f", model.getTemperature()));
		});
	}
}
