package smart_room.version1.room_digitaltwin;


public class RoomPhysicalAssetBridge {

	private RoomModel model;
	private RoomSimulator physicalAssetMock;
	
	public RoomPhysicalAssetBridge(RoomModel model) {
		this.model = model;
		this.physicalAssetMock = new RoomSimulator(model);
	}
	
	public void doCmdStartHeating() {
		physicalAssetMock.startHeating(100);
	}
	
	public void doCmdStartCooling() {
		physicalAssetMock.startCooling(100);
	}
	
	public void doCmdStopWorking() {
		physicalAssetMock.stopWorking();
	}
}
