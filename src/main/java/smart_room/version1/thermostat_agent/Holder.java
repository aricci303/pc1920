package smart_room.version1.thermostat_agent;

public class Holder<T> {

	private T obj;
	
	public Holder() {
	}
		
	public void set(T obj) {
		this.obj = obj;
	}
	
	public T get() {
		return obj;
	}
}
