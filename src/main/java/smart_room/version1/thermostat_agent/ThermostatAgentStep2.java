package smart_room.version1.thermostat_agent;

import java.util.Optional;
import java.util.concurrent.Semaphore;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;

public class ThermostatAgentStep2 extends Thread {

	private static double THRESOLD = 0.5;
	private static String IDLE = "idle";
	private static String COOLING = "cooling";
	private static String HEATING = "heating";
	private double targetGoalTemperature;
	private String smartRoomURL;
	private HttpClient client;
	private int port;
	
	public ThermostatAgentStep2(String smartRoomURL, int port, double targetGoalTemperature) {
		this.targetGoalTemperature = targetGoalTemperature;
		this.smartRoomURL = smartRoomURL;
		this.port = port;
		client = Vertx.vertx().createHttpClient();
		
	}
	
	public void run() {
		log("launched - working with " + smartRoomURL + " - temperature to achieve: " + targetGoalTemperature);		
		while (true) {
			double currentTemp = this.getCurrentTemperature();
			String state = this.getCurrentState();
			log("Idle - State: current temperature: " + currentTemp + " - state: " + state );
			if (Math.abs(currentTemp - targetGoalTemperature) > THRESOLD) {
				boolean tempAchieved = false;
				while (!tempAchieved) {
					currentTemp = this.getCurrentTemperature();
					state = this.getCurrentState();
					log("Acting (State: current temperature: " + currentTemp + " - state: " + state +")" );
					if (currentTemp < (targetGoalTemperature - THRESOLD) && !state.equals(HEATING)) {
						log("too cold: start heating...");
						startHeating();
					} else if (currentTemp > (targetGoalTemperature + THRESOLD) && !state.equals(COOLING)) {
						log("too hot: start cooling...");
						startCooling();
					} else if (Math.abs(currentTemp - targetGoalTemperature) < THRESOLD) {
						log("achieved: stop working");
						if (!state.equals(IDLE)) {
							stopWorking();
						}
						tempAchieved = true;
					}
					waitTimeInMs(500);
				}
			}
			waitTimeInMs(1000);
		}
	}
	
	// actions
	
	protected void log(String msg) {
		System.out.println("[THERMOSTAT] " + msg);
	}
	
	protected void waitTimeInMs(long dt) {
		try {
			Thread.sleep(dt);
		} catch (Exception ex) {};
	}
	
	protected double getCurrentTemperature() {
		JsonObject obj = this.doGetBlocking("/api/properties/temperature");
		return obj.getDouble("temperature");
	}

	protected String getCurrentState() {
		JsonObject obj = this.doGetBlocking("/api/properties/state");
		return obj.getString("state");
	}
	
	protected void startCooling() {
		this.doPostBlocking("/api/actions/start-cooling",Optional.empty());
	}

	protected void startHeating() {
		this.doPostBlocking("/api/actions/start-heating",Optional.empty());
	}

	protected void stopWorking() {
		this.doPostBlocking("/api/actions/stop-working",Optional.empty());
	}
	
	
	// aux
	
	private JsonObject doGetBlocking(String property) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		client.get(port, smartRoomURL, property, response -> {
			// System.out.println("Received response with status code " + response.statusCode());
			response.bodyHandler(bodyHandler -> {
				JsonObject obj = bodyHandler.toJsonObject();
				result.set(obj);
				ready.release();
			});
		}).end();
		try {
			ready.acquire();
			return result.get();
		} catch (Exception ex) {
			return null;
		}
	}

	private void doPostBlocking(String resource, Optional<JsonObject> body) {
		final Semaphore ready = new Semaphore(0);
		Holder<JsonObject> result = new Holder();
		if (!body.isPresent()) {
			client.post(port, smartRoomURL, resource, response -> {
				log("status: " + response.statusCode());
				ready.release();
			}).end();
		} else {
			client.post(port, smartRoomURL, resource, response -> {
				ready.release();
			}).end(body.get().toBuffer());
			
		}
		try {
			ready.acquire();
		} catch (Exception ex) {
		}
	}
	
	public static void main(String[] args) {
		new ThermostatAgentStep2("localhost", 12000, 21).start();
	}	

}
